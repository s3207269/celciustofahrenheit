package nl.utwente.di.temperature;

import java.util.HashMap;
import java.util.Map;

public class Converter {

    public double getBookPrice(String temperature) {
        return (((double) (Integer.parseInt(temperature) * 9) /5) + 32);
    }
}
